import images
import transforms
def main():
    imagen = input("introduce el nombre de la imagen en formato RGB: ")
    funcion = input("introduce la funcion que quieres aplicar: change_colors, rotate_colors, shift...: ")
    img = images.read_img(imagen)
    if funcion in["change_colors", "rotate_colors", "shift", "rotate_right", "mirror", "blur", "grayscale"]:
        if funcion == "change_colors":
            valores = input("Que color quieres cambiar (r,g,b) separados por comas: ")
            list_valores : list[int] = valores.split(", ")
            valores_a_cambiar = tuple(list_valores)
            color = input("A que color quieres cambiar (r,g,b) separados por comas: ")
            list_color_nuevo : list[int] = color.split(",")
            color_nuevo = tuple(list_color_nuevo)
            imagen_cambiada = transforms.change_colors(img, valores_a_cambiar, color_nuevo)
        elif funcion == "rotate_colors":
            incremento = int(input("que incremento vas a aplicar?: "))
            imagen_cambiada = transforms.rotate_colors(img, incremento)

        elif funcion == "shift":
            n_horizontal = int(input("cuanto quieres desplazar horizontalmente?: "))
            n_vertical = int(input("cuanto quieres desplazar verticalmente?: "))
            imagen_cambiada = transforms.shift(img, n_horizontal, n_vertical)


        elif funcion == "rotate_right":
            imagen_cambiada = transforms.rotate_right(img)
        elif funcion == "mirror":
            imagen_cambiada = transforms.mirror(img)
        elif funcion == "blur":
            imagen_cambiada = transforms.blur(img)
        elif funcion == "grayscale":
            imagen_cambiada = transforms.grayscale(img)


        images.write_img(imagen_cambiada, imagen + "trans.png")
    else:
        imagen_cambiada = transforms.funcion(img)
        images.write_img(imagen_cambiada, imagen + "trans.png")

if __name__ == '__main__':
    main()




