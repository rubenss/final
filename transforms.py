import images

def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]):
    for x in range(len(image)):
        for y in range (len(image[x])):
            if image[x][y] == to_change:
                image[x][y] = to_change_to


    return image


def rotate_right(image):

    rotated = []
    for x in range(len(image[0])):
        new_row = []
        for y in range(len(image) - 1, -1, -1):
            pixel = image[y][x]
            new_row.append(pixel)
        rotated.append(new_row)

    return rotated



def mirror(image: list[list[tuple[int, int, int]]]):
    mirrored = []
    for y in image:
        new_y = y[::-1]
        mirrored.append(new_y)
    return mirrored




def rotate_colors(image, increment):

  rotated = []

  for row in image:
    new_row = []
    for pixel in row:
      r, g, b = pixel
      new_r = (r + increment) % 256
      new_g = (g + increment) % 256
      new_b = (b + increment) % 256
      new_pixel = (new_r, new_g, new_b)
      new_row.append(new_pixel)
    rotated.append(new_row)

  return rotated

def blur(image: list[list[tuple[int, int, int]]]):
    for i in range(1, len(image)-1):
        for j in range(1, len(image[0])-1):
            total = [0, 0, 0]
            for x in range(-1, 2):
                for y in range(-1, 2):
                    pixel = image[i+x][j+y]
                    total[0] += pixel[0]
                    total[1] += pixel[1]
                    total[2] += pixel[2]
            image[i][j] = (total[0]//9, total[1]//9, total[2]//9)
    return image


def shift(image, horizontal, vertical):
    shifted = [[(0, 0, 0) for i in range(len(image[0]))] for j in range(len(image))]
    for i in range(len(image)):
        for j in range(len(image[0])):
            new_i = i + vertical
            new_j = j + horizontal
            if new_i < 0 or new_i >= len(image):
                continue
            if new_j < 0 or new_j >= len(image[0]):
                continue
            shifted[new_i][new_j] = image[i][j]
    return shifted


def crop(image, x, y, width, height):
        cropped = []
        for i in range(y, y + height):
            X = []
            for j in range(x, x + width):
                pixel = image[i][j]
                X.append(pixel)
            cropped.append(X)

        return cropped

def grayscale(image: list[list[tuple[int, int, int]]]):
    grayscaled = []

    for x in image:
        new_x = []
        for pixel in x:
            r, g, b = pixel
            grayscale = int(0.2989 * r + 0.5870 * g + 0.1140 * b)
            new_pixel = (grayscale, grayscale, grayscale)
            new_x.append(new_pixel)

        grayscaled.append(new_x)

    return grayscaled



def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float):
    filtered = []

    for x in image:
        new_x = []
        for pixel in x:
            red, green, blue = pixel
            new_red = int(r * red)
            new_green = int(g * green)
            new_blue = int(b * blue)
            new_pixel = (new_red, new_green, new_blue)
            new_x.append(new_pixel)

        filtered.append(new_x)

    return filtered



img_derecha = images.read_img('cafe.jpg')
filtro = rotate_colors(img_derecha, 7)
images.write_img(filtro, 'cafe_rotado_colores.jpg')


















