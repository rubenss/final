import images


def sepia(image):
    sepia_image = []

    for row in image:
        new_row = []
        for pixel in row:
            r, g, b = pixel

            # Calcular componentes sepia
            new_r = (r * 0.393 + g * 0.769 + b * 0.189)
            new_g = (r * 0.349 + g * 0.686 + b * 0.168)
            new_b = (r * 0.272 + g * 0.534 + b * 0.131)

            # Asegurar rango 0-255
            new_r = int(min(max(0, new_r), 255))
            new_g = int(min(max(0, new_g), 255))
            new_b = int(min(max(0, new_b), 255))

            new_pixel = (new_r, new_g, new_b)
            new_row.append(new_pixel)

        sepia_image.append(new_row)

    return sepia_image


img_derecha = images.read_img('cafe.jpg')
filtro = sepia(img_derecha)
images.write_img(filtro, 'cafe_sepia.jpg')
